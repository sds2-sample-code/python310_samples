# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import member
import roll_plate
import job


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# rolled plate begin
rldpl1 = roll_plate.RollPl()
rldpl1.Member = m
rldpl1.Point1 = rldpl1.Member.LeftEnd.Location + rldpl1.Member.TranslateToGlobal( 0, 1, 0 )
rldpl1.Point2 = rldpl1.Point1 + rldpl1.Member.TranslateToGlobal( 60, 1, 0 )
rldpl1.MaterialGrade = 'A36'
rldpl1.Centered = 'No'
rldpl1.TopOperationTypeLeftEnd = 'None'
rldpl1.TopOperationTypeRightEnd = 'None'
rldpl1.BottomOperationTypeLeftEnd = 'None'
rldpl1.BottomOperationTypeRightEnd = 'None'
rldpl1.FabricationMethod = 'Continuous rolling'
rldpl1.Thickness = 0.25
rldpl1.OutsideRadius = 0.25
rldpl1.TaperRadius = 0.25
rldpl1.IncludedAngle = 45.0
rldpl1.BendSegments = 0
rldpl1.WorkpointSlopeDistance = rldpl1.Point1.Distance(rldpl1.Point2)
rldpl1.MaterialSetbackLeftEnd = 0
rldpl1.MaterialSetbackRightEnd = 0
rldpl1.WebCutLeftEnd = 0
rldpl1.WebCutRightEnd = 0
rldpl1.SurfaceFinish = 'Red oxide'
rldpl1.MaterialColor3d = 'Medium_beam'
rldpl1.ReferencePointOffset = (0, 0, 0)
rldpl1.Add()
rldpl1.Rotate(rldpl1.Member, (0, 0, 0))
# rolled plate end
