# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import member
import grate_trd
import job


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# grate tread begin
gt1 = grate_trd.GrateTrd()
gt1.Member = m
gt1.Point1 = gt1.Member.LeftEnd.Location + gt1.Member.TranslateToGlobal( 0, 0, 0 )
gt1.Point2 = gt1.Point1 + gt1.Member.TranslateToGlobal( 60, 0, 0 )
gt1.MaterialGrade = 'A-1011'
gt1.NumberOfStringersInGrate = 9
gt1.BearingBarWidth = 1
gt1.BearingBarThickness = 0.1875
gt1.BearingBarSpacing = 1.1875
gt1.NosingWidth = 1.1875
gt1.CrossBarWidth = 0.5
gt1.CrossBarThickness = 0.1875
gt1.CrossBarSpacing = 4
gt1.EndPlateThickness = 0.1875
gt1.EndPlateWidth = 2.5
gt1.BoltDiameter = 0.5
gt1.NosingToHoleVerticalDistance = 1.75
gt1.NosingToHoleHorizontalDistance = 1.125
gt1.HoleToHoleHorizontalDistance = 7
gt1.Length = 60
gt1.Width = gt1.NosingWidth + gt1.BearingBarSpacing * (gt1.NumberOfStringersInGrate - 1) + gt1.BearingBarThickness
gt1.SurfaceFinish = 'Red oxide'
gt1.MaterialColor3d = 'Medium_beam'
gt1.ReferencePointOffset = (0, 0, 0)
gt1.Add()
gt1.Rotate(gt1.Member, (-90.000000, 0.000000, 0.000000))
# grate tread end
