# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import member
import rolled_section
import job

m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# rolled section begin
hss1 = rolled_section.RolledSection()
hss1.Member = m
hss1.Point1 = hss1.Member.LeftEnd.Location + hss1.Member.TranslateToGlobal( 0, 0, 0 )
hss1.Point2 = hss1.Point1 + hss1.Member.TranslateToGlobal( 60, 0, 0 )
hss1.SectionSize = 'W12x22'
hss1.MaterialGrade = 'A992'
hss1.Centered = 'No'
hss1.TopOperationTypeLeftEnd = 'None'
hss1.TopOperationTypeRightEnd = 'None'
hss1.BottomOperationTypeLeftEnd = 'None'
hss1.BottomOperationTypeRightEnd = 'None'
hss1.IsLongLegVerticalMaterial = 'VT.'
hss1.ToeInOrOut = 'Out'
hss1.RollType = 'None'
hss1.FieldWeldPrepFlagLeftEnd = 'No'
hss1.FieldWeldPrepFlagRightEnd = 'No'
hss1.MomentConnectionWebSetbackLeftEnd = 0
hss1.MomentConnectionWebSetbackRightEnd = 0
hss1.MaterialTwistAngle = 0
hss1.MidOrdinate = 0
hss1.IncludedAngle = 0
hss1.RollingRadius = 0
hss1.SpiralOffset = 0
hss1.WorkpointSlopeDistance = hss1.Point1.Distance(hss1.Point2)
hss1.MaterialSetbackLeftEnd = 0
hss1.MaterialSetbackRightEnd = 0
hss1.WebCutLeftEnd = 0
hss1.WebCutRightEnd = 0
hss1.FlangeCutLeftEnd = 0
hss1.FlangeCutRightEnd = 0
hss1.LeftEndPreparation = 'Standard cut'
hss1.RightEndPreparation = 'Standard cut'
hss1.SurfaceFinish = 'Red oxide'
hss1.MaterialColor3d = 'Medium_beam'
hss1.ReferencePointOffset = (0, 0, 0)
hss1.Add()
hss1.Rotate(hss1.Member, (0.0, 90.0, 0.0))
# rolled section end
