import param
param.Units("feet") #actually means units are inches

import plate_layout
import member
import job

#setup, create a member:
beam = member.Member("Beam")
beam.SectionSize = "W16x26"
beam.LeftEnd.Location = (0, 60, 30)
beam.RightEnd.Location = (120, 120, 0)
beam.Add()

job.ProcessJob()





# flat plate layout begin
fpl3 = plate_layout.PlateLayout()
fpl3.Member = beam
fpl3.pts.append( (fpl3.member.left.location + fpl3.member.trans_to_global( 0, -30, -36 ), 0) )
fpl3.pts.append( (fpl3.member.left.location + fpl3.member.trans_to_global( 0, -36, -24 ), 0) )
fpl3.pts.append( (fpl3.member.left.location + fpl3.member.trans_to_global( 0, -48, -18 ), 0) )
fpl3.pts.append( (fpl3.member.left.location + fpl3.member.trans_to_global( 0, -36, -12 ), 0) )
fpl3.pts.append( (fpl3.member.left.location + fpl3.member.trans_to_global( 0, -30, 0 ), 0) )
fpl3.pts.append( (fpl3.member.left.location + fpl3.member.trans_to_global( 0, -24, -12 ), 0) )
fpl3.pts.append( (fpl3.member.left.location + fpl3.member.trans_to_global( 0, -12, -18 ), 0) )
fpl3.pts.append( (fpl3.member.left.location + fpl3.member.trans_to_global( 0, -24, -24 ), 0) )
fpl3.pts.append( (fpl3.member.left.location + fpl3.member.trans_to_global( 0, -30, -36 ), 0) )
fpl3.MaterialOriginPoint = 'FS'
fpl3.TopOperationTypeLeftEnd = 'None'
fpl3.TopOperationTypeRightEnd = 'None'
fpl3.BottomOperationTypeLeftEnd = 'None'
fpl3.BottomOperationTypeRightEnd = 'None'
fpl3.Thickness = 0.25
fpl3.ReferencePointOffset = (0, 0, 0)
fpl3.Add()
fpl3.Rotate(fpl3.Member, (0.000000, 0.000000, 0.000000))
# flat plate layout end
