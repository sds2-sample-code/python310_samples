# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import member
import rnd_bar
import job


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# rnd bar begin
rb1 = rnd_bar.RndBar()
rb1.Member = m
rb1.Point1 = rb1.Member.LeftEnd.Location + rb1.Member.TranslateToGlobal( 0, 1, 0 )
rb1.Point2 = rb1.Point1 + rb1.Member.TranslateToGlobal( 60, 1, 0 )
rb1.MaterialGrade = 'A36'
rb1.Centered = 'No'
rb1.TopOperationTypeLeftEnd = 'None'
rb1.TopOperationTypeRightEnd = 'None'
rb1.BottomOperationTypeLeftEnd = 'None'
rb1.BottomOperationTypeRightEnd = 'None'
rb1.Width = 0.75
rb1.BarDiameter = 0.75  # A round bar with this diameter must exist in the material file
rb1.Thickness = 0
rb1.WorkpointSlopeDistance = rb1.Point1.Distance(rb1.Point2)
rb1.MaterialSetbackLeftEnd = 0
rb1.MaterialSetbackRightEnd = 0
rb1.WebCutLeftEnd = 0
rb1.WebCutRightEnd = 0
rb1.ThreadTypeLeftEnd = 'None'
rb1.ThreadTypeRightEnd = 'None'
rb1.SurfaceFinish = 'Red oxide'
rb1.MaterialColor3d = 'Medium_beam'
rb1.ReferencePointOffset = (0, 0, 0)
rb1.Add()
rb1.Rotate(rb1.Member, (0, 0, 0))
# rnd bar end
