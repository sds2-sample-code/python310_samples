# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import member
import sqr_bar
import job


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# sqr bar begin
sb1 = sqr_bar.SqrBar()
sb1.Member = m
sb1.Point1 = sb1.Member.LeftEnd.Location + sb1.Member.TranslateToGlobal( 0, 1, 0 )
sb1.Point2 = sb1.Point1 + sb1.Member.TranslateToGlobal( 60, 1, 0 )
sb1.MaterialGrade = 'A36'
sb1.Centered = 'No'
sb1.TopOperationTypeLeftEnd = 'None'
sb1.TopOperationTypeRightEnd = 'None'
sb1.BottomOperationTypeLeftEnd = 'None'
sb1.BottomOperationTypeRightEnd = 'None'
sb1.Width = 0.75
sb1.Thickness = 0
sb1.WorkpointSlopeDistance = sb1.Point1.Distance(sb1.Point2)
sb1.MaterialSetbackLeftEnd = 0
sb1.MaterialSetbackRightEnd = 0
sb1.WebCutLeftEnd = 0
sb1.WebCutRightEnd = 0
sb1.SurfaceFinish = 'Red oxide'
sb1.MaterialColor3d = 'Medium_beam'
sb1.ReferencePointOffset = (0, 0, 0)
sb1.Add()
sb1.Rotate(sb1.Member, (0, 0, 0))
# sqr bar end
