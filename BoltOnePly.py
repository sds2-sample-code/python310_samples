# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import member
import rect_plate
import job
import bolt_add
import hole_add


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W44x335'
m.Add()

job.ProcessJob()

# rectangular plate begin
p1 = rect_plate.RectPlate()
p1.Member = m
p1.Point1 = p1.Member.LeftEnd.Location + p1.Member.TranslateToGlobal( 0, 6, 0 )
p1.Point2 = p1.Point1 + p1.Member.TranslateToGlobal( 60, 0, 0 )
p1.MaterialGrade = 'A36'
p1.MaterialOriginPoint = 'FS'
p1.TopOperationTypeLeftEnd = 'None'
p1.TopOperationTypeRightEnd = 'None'
p1.BottomOperationTypeLeftEnd = 'None'
p1.BottomOperationTypeRightEnd = 'None'
p1.Width = 6.0
p1.Thickness = 0.25
p1.WorkpointSlopeDistance = p1.Point1.Distance(p1.Point2)
p1.MaterialSetbackLeftEnd = 0
p1.MaterialSetbackRightEnd = 0
p1.WebCutLeftEnd = 0
p1.WebCutRightEnd = 0
p1.SurfaceFinish = 'Red oxide'
p1.MaterialColor3d = 'Medium_beam'
p1.ReferencePointOffset = (0, 0, 0)
p1.Add()
p1.Rotate(p1.Member, (0, 0, 0))
# rectangular plate end
# hole group add begin
hole1 = hole_add.Hole()
hole1.Material = [(1, 1, -1), ]  # (1, 1, -1) says member 1, material 1 (zero based index, so second material), [p1] would work too
hole1.Point = p1.Member.LeftEnd.Location + p1.Member.TranslateToGlobal( 0, 6, 0 )
hole1.Type = 'Standard Round'
hole1.Matchable = 'Yes'
hole1.MaterialFace = 'FS Face'
hole1.ShouldBeValid = 'Yes'
hole1.ReferenceOffsetX = 2
hole1.ReferenceOffsetY = 2.5
hole1.SpacingX = 2
hole1.SpacingY = 2
hole1.GroupRotation = 0
hole1.Locate = 'Above Right'
hole1.Columns = 3
hole1.Rows = 2
hole1.BoltType = 'AUTO'
hole1.PlugType = 'No Plug'
hole1.BoltDiameter = 0.75
hole1.Diameter = hole1.CalculateHoleSize()
hole1.BothSides = 'Yes'
hole1.ShowWindow = 'No'
hole1.Create()
# hole group add end
# bolt add begin
bolts = bolt_add.Bolt()
bolts.Holes = hole1.HolesAdded()
bolts.Diameter = 0.75
bolts.PrimaryNutType = 'Heavy hex'
bolts.SecondaryNutType = 'None'
bolts.BoltType = 'AUTO'
bolts.IsFieldBolt = 'Field'
bolts.IsTensionControl = 'No'
bolts.Finish = 'Black'
bolts.ShowWindow = 'No'
bolts.Direction = 'Out'
bolts.AddOnePly()
# bolt add end
