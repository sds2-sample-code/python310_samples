import param
param.Units("feet") #actually means units are inches

import member
import job
import bolt_add

#setup, create a member:
beam = member.Member("Beam")
beam.SectionSize = "W16x26"
beam.LeftEnd.Location = (0, 60, 30)
beam.RightEnd.Location = (120, 120, 0)
beam.Add()

job.ProcessJob()


# bolt add begin
bolt1 = bolt_add.Bolt()
bolt1.Member = beam
bolt1.HeadPoint = (0, 20.283902, 0)
bolt1.NutPoint = (8, 20.283902, 0)
bolt1.Diameter = 1
bolt1.Length = 2.75
bolt1.Grip = 1.75
bolt1.PrimaryNutType = 'Heavy hex'
bolt1.PrimaryNutGrade = 'A563'
bolt1.SecondaryNutType = 'None'
bolt1.SecondaryNutGrade = 'A563'
bolt1.BoltType = 'A325N'
bolt1.IsFieldBolt = 'Field'
bolt1.IsTensionControl = 'Yes'
bolt1.Boltless = 'No'
bolt1.Finish = 'Black'
bolt1.HillsideWasherAngle = 0
bolt1.ShowWindow = 'No'
bolt1.Direction = 'Out'
bolt1.AddPointToPoint()
# bolt add end
