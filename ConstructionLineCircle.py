# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
from cons_circle import ConsCircle
from cons_line import ConsLine

# construction line begin
cl1 = ConsLine()
cl1.Point1 = (0, 0, 0)
cl1.Rotation = 0
cl1.PenNumber = 'Blue'
cl1.Add()
# construction line end
# construction circle begin
cc1 = ConsCircle()
cc1.Center = (0, 0, 0)
cc1.Radius = 12
cc1.PenNumber = 'Blue'
cc1.Add()
# construction circle end
