import param
param.Units("feet") #actually means units are inches

import plate_layout
import member
import job

#setup, create a member:
beam = member.Member("Beam")
beam.SectionSize = "W16x26"
beam.LeftEnd.Location = (0, 60, 30)
beam.RightEnd.Location = (120, 120, 0)
beam.Add()

job.ProcessJob()


# bent plate layout begin
bpl1 = plate_layout.BntPlateLayout()
bpl1.Member = beam
bpl1.pts.append( (bpl1.member.left.location + bpl1.member.trans_to_global( 0, -17.656717, 0 ), 0) )
bpl1.pts.append( (bpl1.member.left.location + bpl1.member.trans_to_global( 0, -34.533552, 0 ), 0.5) )
bpl1.pts.append( (bpl1.member.left.location + bpl1.member.trans_to_global( 0, -40.5199, -5.986348 ), 1.0) )
bpl1.pts.append( (bpl1.member.left.location + bpl1.member.trans_to_global( 0, -32.582963, -13.923286 ), 0.5) )
bpl1.pts.append( (bpl1.member.left.location + bpl1.member.trans_to_global( 0, -17.99595, -13.923286 ), 0) )
bpl1.MaterialOriginPoint = 'FS'
bpl1.TopOperationTypeLeftEnd = 'None'
bpl1.TopOperationTypeRightEnd = 'None'
bpl1.BottomOperationTypeLeftEnd = 'None'
bpl1.BottomOperationTypeRightEnd = 'None'
bpl1.Width = 12
bpl1.Thickness = 0.25
bpl1.WorkpointSlopeDistance = 0
bpl1.MaterialSetbackLeftEnd = 0
bpl1.MaterialSetbackRightEnd = 0
bpl1.WebCutLeftEnd = 0
bpl1.WebCutRightEnd = 0
bpl1.ReferencePointOffset = (0, 0, 0)
bpl1.Add()
bpl1.Rotate(bpl1.Member, (0.000000, 0.000000, 0.000000))
# bent plate layout end
