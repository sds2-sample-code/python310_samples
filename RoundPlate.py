# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import job
import member
import rnd_plate

m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# rounded plate begin
rndpl1 = rnd_plate.RndPlate()
rndpl1.Member = m
rndpl1.Point1 = rndpl1.Member.LeftEnd.Location + rndpl1.Member.TranslateToGlobal( 0, 0, 0 )
rndpl1.Point2 = rndpl1.Point1 + rndpl1.Member.TranslateToGlobal( 30, 0, 0 )
rndpl1.MaterialGrade = 'A36'
rndpl1.MaterialOriginPoint = 'FS'
rndpl1.TopOperationTypeLeftEnd = 'None'
rndpl1.TopOperationTypeRightEnd = 'None'
rndpl1.BottomOperationTypeLeftEnd = 'None'
rndpl1.BottomOperationTypeRightEnd = 'None'
rndpl1.Thickness = 0.25
rndpl1.PlateDiameter = rndpl1.Point1.Distance(rndpl1.Point2) * 2.0
rndpl1.SurfaceFinish = 'Red oxide'
rndpl1.MaterialColor3d = 'Medium_beam'
rndpl1.ReferencePointOffset = (0, 0, 0)
rndpl1.Add()
rndpl1.Rotate(rndpl1.Member, (0.0, 0.0, 0.0))
# rounded plate end
