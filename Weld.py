# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import member
import rect_plate
import job
import weld_add


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# rectangular plate begin
p1 = rect_plate.RectPlate()
p1.Member = m
p1.Point1 = p1.Member.LeftEnd.Location + p1.Member.TranslateToGlobal( 0, 0, 0 )
p1.Point2 = p1.Point1 + p1.Member.TranslateToGlobal( 60, 0, 0 )
p1.MaterialGrade = 'A36'
p1.MaterialOriginPoint = 'FS'
p1.TopOperationTypeLeftEnd = 'None'
p1.TopOperationTypeRightEnd = 'None'
p1.BottomOperationTypeLeftEnd = 'None'
p1.BottomOperationTypeRightEnd = 'None'
p1.Width = 6.0
p1.Thickness = 0.25
p1.WorkpointSlopeDistance = p1.Point1.Distance(p1.Point2)
p1.MaterialSetbackLeftEnd = 0
p1.MaterialSetbackRightEnd = 0
p1.WebCutLeftEnd = 0
p1.WebCutRightEnd = 0
p1.SurfaceFinish = 'Red oxide'
p1.MaterialColor3d = 'Medium_beam'
p1.ReferencePointOffset = (0, 0, 0)
p1.Add()
p1.Rotate(p1.Member, (-90, 0, 0))
# rectangular plate end
# weld add begin
weld1 = weld_add.Weld()
weld1.Material = [p1]
weld1.WeldTo = m.MainMaterial()
weld1.ArrowSize = 0.187500
weld1.OtherSize = 0.000000
weld1.FieldWeld = 'No'
weld1.WeldAllAround = 'No'
weld1.SpacerBarRequired = 'No'
weld1.NonPrequalifiedWeld = 'Yes'
weld1.JointType = 'None'
weld1.Penetration = 'None'
weld1.Process = 'None'
weld1.Position = 'None'
weld1.MaximumGap = 0
weld1.WeldInside = 'No'
weld1.one_weld_per_segment = 'No'
weld1.min_length = 0
weld1.generate_fillet = 'Yes'
weld1.generate_flare = 'No'
weld1.generate_plug = 'No'
weld1.generate_v_groove = 'No'
weld1.generate_bevel_groove = 'No'
weld1.ArrowWeldType = 'Fillet'
weld1.ArrowLeftSetback = 0
weld1.ArrowRightSetback = 0
weld1.ArrowRootFace = 0
weld1.ArrowRootOpening = 0
weld1.arrow_effective_throat = 0
weld1.ArrowWeldContourDescription = 'None'
weld1.ArrowGrooveAngle = 0
weld1.ArrowFilletBackupWeld = 'No'
weld1.Stitch = 'No'
weld1.ArrowStitchLength = 0
weld1.ArrowStitchSpacing = 0
weld1.ArrowLeftTermination = 0
weld1.ArrowRightTermination = 0
weld1.OtherWeldTypeIndex = 'None'
weld1.OtherLeftSetback = 0
weld1.OtherRightSetback = 0
weld1.OtherRootFace = 0
weld1.OtherRootOpening = 0
weld1.other_effective_throat = 0
weld1.OtherContour = 'None'
weld1.OtherGrooveAngle = 0
weld1.OtherFilletBack = 'No'
weld1.other_stitch = 'None'
weld1.OtherStitchLength = 0
weld1.OtherStitchSpacing = 0
weld1.OtherLeftTermination = 0
weld1.OtherRightTermination = 0
weld1.WeldSymbolTailText = ""
weld1.ShowWindow = 'No'
weld1.Create()
# weld add end
