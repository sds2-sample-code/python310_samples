# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import member
import rect_plate
import job
import hole_add
import bolt_add


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W44x335'
m.Add()

job.ProcessJob()

# rectangular plate begin
p1 = rect_plate.RectPlate()
p1.Member = m
p1.Point1 = p1.Member.LeftEnd.Location + p1.Member.TranslateToGlobal( 0, 0, 0 )
p1.Point2 = p1.Point1 + p1.Member.TranslateToGlobal( 60, 0, 0 )
p1.MaterialGrade = 'A36'
p1.MaterialOriginPoint = 'FS'
p1.TopOperationTypeLeftEnd = 'None'
p1.TopOperationTypeRightEnd = 'None'
p1.BottomOperationTypeLeftEnd = 'None'
p1.BottomOperationTypeRightEnd = 'None'
p1.Width = 6.0
p1.Thickness = 0.25
p1.WorkpointSlopeDistance = p1.Point1.Distance(p1.Point2)
p1.MaterialSetbackLeftEnd = 0
p1.MaterialSetbackRightEnd = 0
p1.WebCutLeftEnd = 0
p1.WebCutRightEnd = 0
p1.SurfaceFinish = 'Red oxide'
p1.MaterialColor3d = 'Medium_beam'
p1.ReferencePointOffset = (0, 0, 0)
p1.Add()
p1.Rotate(p1.Member, (-90, 0, 0))
# rectangular plate end
# hole group add begin
hole1 = hole_add.Hole()
hole1.Material = [(1, 1, -1), ]  # (1, 1, -1) says member 1, material 1 (zero based index, so second material), [p1] would work too
hole1.Point = p1.Member.LeftEnd.Location + p1.Member.TranslateToGlobal( 0, 0, 0 )
hole1.Type = 'Standard Round'
hole1.Matchable = 'Yes'
# MaterialFace is optional but if Point1 is on an edge you will want to specify one of
# "Unknown", "Top Face", "Web NS", "Bottom Face", "Web FS", "NS Face", "FS Face"
hole1.MaterialFace = 'FS Face'
hole1.ShouldBeValid = 'Yes'
hole1.ReferenceOffsetX = 2
hole1.ReferenceOffsetY = 2.5
hole1.SpacingX = 2
hole1.SpacingY = 2
hole1.GroupRotation = 0
hole1.Locate = 'Above Right'
hole1.Columns = 3
hole1.Rows = 2
hole1.BoltType = 'AUTO'
hole1.PlugType = 'No Plug'
hole1.BoltDiameter = 0.75
hole1.Diameter = hole1.CalculateHoleSize()
hole1.BothSides = 'Yes'
hole1.ShowWindow = 'No'
hole1.Create()
# hole group add end
# hole group add begin
hole2 = hole_add.Hole()
hole2.Material = [(1, 0, -1), ]  # (1, 0, -1) says member 1, material 0 (zero based index, so first/main material)
hole2.Holes = [hole1, ]
hole2.Type = 'Standard Round'
hole2.Matchable = 'Yes'
hole2.ShouldBeValid = 'Yes'
hole2.Diameter = hole2.calc_hole_size()
hole2.BothSides = 'Yes'
hole2.ShowWindow = 'No'
hole2.Create()
# hole group add end
# bolt add begin
bolt2 = bolt_add.Bolt()
bolt2.Material = hole1.Material
bolt2.Match = hole2.Material
bolt2.Diameter = 0.75
bolt2.PrimaryNutType = 'Heavy hex'
bolt2.SecondaryNutType = 'None'
bolt2.BoltType = 'AUTO'
bolt2.IsFieldBolt = 'Field'
bolt2.IsTensionControl = 'No'
bolt2.Finish = 'Black'
bolt2.ShowWindow = 'No'
bolt2.Direction = 'Out'
bolt2.AddMatch()
# bolt add end
