import param
param.Units("feet") #actually means units are inches

import member
import mtrl_cut
import job
import point


#setup, create a member:
beam = member.Member("Beam")
beam.SectionSize = "W16x26"
beam.LeftEnd.Location = (0, 60, 30)
beam.RightEnd.Location = (120, 120, 0)
beam.Add()

#process, we'll use the main material as an example to cut
job.ProcessJob()

# mtrl cut begin
mcut = mtrl_cut.MtrlCut()
mcut.Material = beam.material(0)
#default rotation would be the xy plane, we want the xz plane of the beam
#so we're rotating 90 degrees on the X:
mcut.Rotate = (90, 0, 0)
mcut.DepthIn = 3
mcut.DepthOut = 3

mcut.pts.append( ((mcut.mtrl.location + mcut.mtrl.trans_to_global( -3, 0, 1)), 0.0) )
mcut.pts.append( ((mcut.mtrl.location + mcut.mtrl.trans_to_global( 3, 0, 1 )), 0.0) )
mcut.pts.append( ((mcut.mtrl.location + mcut.mtrl.trans_to_global( 3, 0, 12)), 0.0) )
mcut.pts.append( ((mcut.mtrl.location + mcut.mtrl.trans_to_global( -3, 0, 12)),0.0) )
mcut.pts.append( ((mcut.mtrl.location + mcut.mtrl.trans_to_global( -3, 0, 1)), 0.0) )
mcut.Cut("Layout")



#second example, hitting the web of this beam with some 5 ninja star reviewed castellation:
for i in range(1, 6):
    mcut = mtrl_cut.MtrlCut()
    mcut.Material = beam.material(0)
    #now we want the default rotation so we can cut the web:
    mcut.Rotate = (0, 0, 0)
    mcut.DepthIn = 3
    mcut.DepthOut = 3

    star = [(3, -1, 0),
            (4, -3, 0),
            (6, -4, 0),
            (4, -5, 0),
            (3, -7, 0),
            (2, -5, 0),
            (0, -4, 0),
            (2, -3, 0),
            (3, -1, 0)]

    for p in star:
        mcut.pts.append((mcut.mtrl.location + mcut.mtrl.trans_to_global(i*12+p[0], -3+p[1], 0), 0.0))
    mcut.Cut("Layout")
# mtrl cut end
