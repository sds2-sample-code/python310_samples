# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import member
import rect_plate
import mtrl_fit
import job


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# rectangular plate begin
p1 = rect_plate.RectPlate()
p1.Member = m
p1.Point1 = p1.Member.LeftEnd.Location + p1.Member.TranslateToGlobal( 6, 0.5, 1 )
p1.Point2 = p1.Point1 + p1.Member.TranslateToGlobal( 6, 0, 0 )
p1.MaterialGrade = 'A36'
p1.MaterialOriginPoint = 'NS'
p1.TopOperationTypeLeftEnd = 'None'
p1.TopOperationTypeRightEnd = 'None'
p1.BottomOperationTypeLeftEnd = 'None'
p1.BottomOperationTypeRightEnd = 'None'
p1.Width = 6.0
p1.Thickness = 1.5
p1.WorkpointSlopeDistance = p1.Point1.Distance(p1.Point2)
p1.MaterialSetbackLeftEnd = 0
p1.MaterialSetbackRightEnd = 0
p1.WebCutLeftEnd = 0
p1.WebCutRightEnd = 0
p1.SurfaceFinish = 'Red oxide'
p1.MaterialColor3d = 'Medium_beam'
p1.ReferencePointOffset = (0, 0, 0)
p1.Add()
p1.Rotate(p1.Member, (-90, 0, 0))
# rectangular plate end
# mtrl fit begin
mfit1 = mtrl_fit.MtrlFit()
mfit1.Material = m.MainMaterial()
mfit1.To = [p1]
mfit1.FrameType = 'Field weld'
mfit1.CutClear = 1.0/16.0
mfit1.ShowWindow = 'No'
mfit1.Fit("Exact")
# mtrl fit end
