# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import grate
import member
import job


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# grating begin
gr1 = grate.Grate()
gr1.Member = m
gr1.Point1 = gr1.Member.LeftEnd.Location + gr1.Member.TranslateToGlobal( 0, 0, 0 )
gr1.Point2 = gr1.Point1 + gr1.Member.TranslateToGlobal( 60, 0, 0 )
gr1.MaterialGrade = 'A-1011'
gr1.NumberOfStringersInGrate = 21
gr1.BearingBarWidth = 1.5
gr1.BearingBarThickness = 0.1875
gr1.BearingBarSpacing = 1.1875
gr1.CrossBarWidth = 0.5
gr1.CrossBarThickness = 0.1875
gr1.CrossBarSpacing = 4
gr1.LeftEndCutAngle = 0
gr1.RightEndCutAngle = 0
gr1.LeftEndBandThickness = 0.1875
gr1.RightEndBandThickness = 0.1875
gr1.Length = 60
gr1.Width = gr1.BearingBarSpacing * (gr1.NumberOfStringersInGrate - 1) + gr1.BearingBarThickness
gr1.SurfaceFinish = 'Red oxide'
gr1.MaterialColor3d = 'Medium_beam'
gr1.ReferencePointOffset = (0, 0, 0)
gr1.Add()
gr1.Rotate(gr1.Member, (-90, 0, 0))
# grating end
