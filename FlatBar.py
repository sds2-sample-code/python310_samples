# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import flat_bar
import job
import member


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# flat bar begin
f1 = flat_bar.FlatBar()
f1.Member = m
f1.Point1 = f1.Member.LeftEnd.Location + f1.Member.TranslateToGlobal( 0, 1.0/8.0, 0 )
f1.Point2 = f1.Point1 + f1.Member.TranslateToGlobal( 60, 1.0/8.0, 0 )
f1.MaterialGrade = 'A36'
f1.Centered = 'No'
f1.TopOperationTypeLeftEnd = 'None'
f1.TopOperationTypeRightEnd = 'None'
f1.BottomOperationTypeLeftEnd = 'None'
f1.BottomOperationTypeRightEnd = 'None'
f1.RollType = 'None'
f1.Width = 2
f1.Thickness = 0.25
f1.MaterialTwistAngle = 0
f1.MidOrdinate = 0
f1.IncludedAngle = 0
f1.RollingRadius = 0
f1.SpiralOffset = 0
f1.WorkpointSlopeDistance = f1.Point1.Distance(f1.Point2)
f1.MaterialSetbackLeftEnd = 0
f1.MaterialSetbackRightEnd = 0
f1.WebCutLeftEnd = 0
f1.WebCutRightEnd = 0
f1.SurfaceFinish = 'Red oxide'
f1.MaterialColor3d = 'Medium_beam'
f1.ReferencePointOffset = (0, 0, 0)
f1.Add()
f1.Rotate(f1.Member, (-90.000000, 0.000000, 0.000000))
# flat bar end

