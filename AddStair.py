import param
param.Units("feet") #actually means units are inches
import member
import job



# add stair begin
stair1 = member.Member("Stair")
stair1.LeftEnd.Location = (0, 0, 0)
stair1.RightEnd.Location = (120, 0, 120)
stair1.erected_date = "NOT SET"
stair1.existing = "No"
stair1.fab_actual = "NOT SET"
stair1.fab_complete = "NOT SET"
stair1.fab_projected = "NOT SET"
stair1.held_date = "NOT SET"
stair1.hold_status = "Not held"
stair1.is_galvanized = "No"
stair1.memb_category = ""
stair1.memb_route1 = ""
stair1.memb_route2 = ""
stair1.memb_route3 = ""
stair1.memb_route4 = ""
stair1.model_complete = "NOT SET"
stair1.rcvd_appr = "NOT SET"
stair1.rcvd_jobsite = "NOT SET"
stair1.reason_held = ""
stair1.sent_appr = "NOT SET"
stair1.ship_actual = "NOT SET"
stair1.ship_projected = "NOT SET"
stair1.short_rev_desc = "No Revision"

#these are stair specific items:
stair1.left.ns_end_cond = "No return"
stair1.left.fs_end_cond = "No return"
stair1.left.ns_cope_depth_top = 0.0
stair1.left.ns_cope_depth_bot = 0.0
stair1.left.fs_cope_depth_top = 0.0
stair1.left.fs_cope_depth_bot = 0.0
stair1.left.ns_cope_len_top = 0.0
stair1.left.ns_cope_len_bot = 0.0
stair1.left.fs_cope_len_top = 0.0
stair1.left.fs_cope_len_bot = 0.0
stair1.left.ns_cap_pl_thick = 0.0
stair1.left.fs_cap_pl_thick = 0.0
stair1.left.ns_supp_to_wkpt = 0.0
stair1.left.fs_supp_to_wkpt = 0.0
stair1.left.ns_slab_to_top_rtn = 0.0
stair1.left.fs_slab_to_top_rtn = 0.0
stair1.left.ns_bolt_to_floor_clearance = 0.0
stair1.left.fs_bolt_to_floor_clearance = 0.0
stair1.left.ns_cap_setback = 0.0
stair1.left.fs_cap_setback = 0.0
stair1.left.ns_return_setback = 0.0
stair1.left.fs_return_setback = 0.0
#stair1.right has all the same properties as left
stair1.add()
# add stair end


job.ProcessJob()
