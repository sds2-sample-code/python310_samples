import param
param.Units("feet") #actually means units are inches
import member
import job

# add structural members begin
column1 = member.Member("Column")
print(column1.MemberNumber) #at this point it is -1, this member isn't added yet
column1.SectionSize = "W10x49"
column1.LeftEnd.Location = (0, 0, 0)
column1.RightEnd.Location = (0, 0, 120) #10 foot in the air
column1.LeftEnd.input_conn_type = "User base/cap plate" #this is a user base plate
"""
Valid options for input_conn_type (most of these, are member type dependent):

"Auto standard"
"Plain end"
"Clip angle"
"Bent plate"
"End plate"
"User base/cap plate"
"Shear"
"Seated"
"Splice plate"
"Vbrace plate"
"Hbrace plate"
"Bearing"
"Auto base/cap plate"
"User defined"
"Welded"
"Fully welded moment"
"Flush framed shear"
"""
column1.LeftEnd.auto_compression = "No"
column1.LeftEnd.compression = 10.0
#these are defaults, putting them hear for reference:
column1.LeftEnd.auto_minus_dim = "Yes"
column1.LeftEnd.auto_moment = "Yes"
column1.LeftEnd.auto_setback = "Yes"
column1.LeftEnd.auto_shear = "Yes"

column1.RightEnd.input_conn_type = "Auto standard"
column1.Add() #this is when the column is created in the model
print(column1.MemberNumber) #at this point it has a valid member number

beam2 = member.Member("Beam")
beam2.SectionSize = "W16x26"
beam2.LeftEnd.Location = (0, 0, 120)
beam2.RightEnd.Location = (120, 0, 120)
beam2.LeftEnd.input_conn_type = "Clip angle"
#these are all defaults, showing for reference:
beam2.LeftEnd.auto_compression = "No"
beam2.LeftEnd.auto_minus_dim = "Yes"
beam2.LeftEnd.auto_moment = "Yes"
beam2.LeftEnd.auto_setback = "Yes"
beam2.LeftEnd.auto_shear = "Yes"
beam2.LeftEnd.auto_tension = "Yes"
#if auto_compression were "No", then you'd want a positive value here
beam2.LeftEnd.compression = 0.0
beam2.LeftEnd.shear = 0.0
beam2.LeftEnd.conn_setback = 0.0
beam2.LeftEnd.dihedral = 0.0
beam2.LeftEnd.end_cut = "Standard cut"
beam2.LeftEnd.field_clear = 0.0
beam2.LeftEnd.flange_cut_angle = 0.0
beam2.LeftEnd.minus_dim = 0.0
beam2.LeftEnd.offset_dim = 0.0
beam2.LeftEnd.rotation = 0.0
beam2.LeftEnd.set_conn_setback = "No"
beam2.LeftEnd.set_field_clear = "No"
beam2.LeftEnd.setback = 0.0
beam2.LeftEnd.shear = 0.0
beam2.LeftEnd.tension = 0.0
beam2.LeftEnd.web_cut_angle = 0.0
beam2.RightEnd.input_conn_type = "Shear"
beam2.Add()

vbrace3 = member.Member("VBrace")
vbrace3.SectionSize = "L4x4x1/2"
vbrace3.LeftEnd.Location = (0, 0, 112)
vbrace3.RightEnd.Location = (120, 0, 0)
vbrace3.Add()

column4 = member.Member("Column")
column4.SectionSize = "W10x49"
column4.LeftEnd.Location = (0, 120, 0)
column4.RightEnd.Location = (0, 120, 120)
column4.Add()

beam5 = member.Member("Beam")
beam5.SectionSize = "W16x26"
beam5.LeftEnd.Location = (0, 120, 120)
beam5.RightEnd.Location = (120, 120, 120)
beam5.Add()

joist6 = member.Member("Joist")
joist6.SectionSize = "10K1"
joist6.LeftEnd.input_conn_type = "Bearing"
joist6.RightEnd.input_conn_type = "Bearing"
joist6.LeftEnd.Location = (60, 0, 120)
joist6.RightEnd.Location = (60, 120, 120)
joist6.Add()
# add structural members end

#none of the members we added are processed, this will process them all:
job.ProcessJob()
