# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import member
import turnbuckle
import job


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# trnbkle begin
tb1 = turnbuckle.Turnbuckle()
tb1.Member = m
tb1.Point1 = tb1.Member.LeftEnd.Location + tb1.Member.TranslateToGlobal( 0, 1, 0 )
tb1.Point2 = tb1.Point1 + tb1.Member.TranslateToGlobal( 60, 1, 0 )
tb1.SectionSize = 'TB3/8x6'
tb1.MaterialGrade = 'C-1035'
tb1.LeftThreadType = 'None'
tb1.RightThreadType = 'None'
tb1.SurfaceFinish = 'Red oxide'
tb1.MaterialColor3d = 'Medium_beam'
tb1.ReferencePointOffset = (0, 0, 0)
tb1.Add()
tb1.Rotate(tb1.Member, (0.000000, 0.000000, 0.000000))
# trnbkle end
