# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import member
import shr_stud
import job


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# shear stud begin
ss1 = shr_stud.ShrStud()
ss1.Member = m
ss1.Point1 = ss1.Member.LeftEnd.Location + ss1.Member.TranslateToGlobal( 0, 1, 0 )
ss1.Point2 = ss1.Point1 + ss1.Member.TranslateToGlobal( 60, 1, 0 )
ss1.MaterialGrade = 'A108'
ss1.Diameter = 0.5
ss1.HeadThickness = 0.3125
ss1.HeadDiameter = 1
ss1.Length = 60
ss1.SurfaceFinish = 'Red oxide'
ss1.MaterialColor3d = 'Medium_beam'
ss1.ReferencePointOffset = (0, 0, 0)
ss1.Add()
ss1.Rotate(ss1.Member, (0.000000, 0.000000, 0.000000))
# shear stud end
