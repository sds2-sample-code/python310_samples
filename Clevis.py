# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")
import member
import clevis
import job


m = member.Member('Beam')
m.LeftEnd.Location = (0, 0, 0)
m.RightEnd.Location = (60, 0, 0)
m.SectionSize = 'W12x22'
m.Add()

job.ProcessJob()

# clevis begin
cl1 = clevis.Clevis()
cl1.Member = m
cl1.Point1 = cl1.Member.LeftEnd.Location + cl1.Member.TranslateToGlobal( 0, 1, 0 )
cl1.Point2 = cl1.Point1 + cl1.Member.TranslateToGlobal( 6, 1, 0 )
cl1.SectionSize = 'CV2'
cl1.MaterialGrade = 'C-1035'
cl1.Grip = 0.75
cl1.PinDiameter = 0.75
cl1.RodDiameter = 0.75
cl1.ThreadType = 'None'
cl1.SurfaceFinish = 'Red oxide'
cl1.MaterialColor3d = 'Medium_beam'
cl1.ReferencePointOffset = (0, 0, 0)
cl1.Add()
cl1.Rotate(cl1.Member, (0.000000, 0.000000, 0.000000))
# clevis end
