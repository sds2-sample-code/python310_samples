# The following code overrides dialog.dialog.DialogBase.done
# so that automated testing can run this script, bring up
# the screen, optionally interact with it (not shown here), and
# Quit(True) or Quit(False) from the edit screen

import dialog.dialog
import dialog.simulate

original_done = dialog.dialog.DialogBase.done

def click_ok(dlg):
    yield None  # wait for idle events to process
    dlg.Quit(True)

def testing_done(cls):
    s = dialog.simulate.Simulator(cls, click_ok, cls)
    return original_done(cls)

dialog.dialog.DialogBase.done = testing_done



# Generally, all parametrics, including ones run in metric jobs,
# should explicitly set param.Units() to an imperial dimension
# style to tell parametrics that the units used in the Python
# script are inches. The 'feet' dimension style is the normal
# style SDS2 uses for imperial jobs.

# param.Units('metric') was an idea from log ago that Python
# developers who typically worked in metric jobs would prefer
# working in the units of their jobs. However, not all SDS2
# Python APIs support this functionality, e.g. the model module.
# The least error prone approach is to work in inches regardless
# of the job. Just because a unit is in inches does not mean
# the inches can't be set to an exact millimeter.
import param
param.Units("feet")

# Dialog begin
from dialog import Dialog
from dialog.checkbox import CheckButtons
from dialog.checkbox import Checkbox
from dialog.choose_file import ChooseFile
from dialog.choose_file import ChooseMtrl
from dialog.combobox import Combobox
from dialog.date import DateEntry
from dialog.dimension import DimensionEntry
from dialog.entry import FloatEntry
from dialog.entry import ReadOnlyEntry
from dialog.entry import StrEntry
from dialog.frame import Column
from dialog.frame import Frame
from dialog.image import Image
from dialog.label import Label
from dialog.tabset import Tab
from dialog.tabset import Tabset

Dialog0 = Dialog( 'Hello World!' );
Entry1 = FloatEntry( Dialog0, 'fl_name', label="Float field", default=3.0 )
Entry2 = DimensionEntry( Dialog0, 'dim_name', label="Dimension field", default="1-2 1/2" )
Entry3 = StrEntry( Dialog0, 'str_name', label="String field", default="Default String" )
Entry4 = DateEntry( Dialog0, 'date_name', label="Date field", default="**NOT SET**" )
Menu5 = Combobox( Dialog0, 'menu_name', ( "Item 1", "Item 2", "Item 3" ), label="Menu field", default="Item 1" )
Check_boxes6 = CheckButtons( Dialog0, 'checkbuttons_name', ( 1, 2, 3 ), label="Button group", default=(1,) )
Check_box7 = Checkbox( Dialog0, 'button_name', label="check", default=False )
Entry8 = ReadOnlyEntry( Dialog0, label="Label field", displayed_value="Default value" )
Header_line9 = Label( Dialog0, "Header Line" )
Image10 = Image( Dialog0, '/path/image.gif' )
File_browse11 = ChooseFile( Dialog0, 'file_name', label="Enter filename", save=False, default="/path" )
Material_browse12 = ChooseMtrl( Dialog0, 'section_size', ( "W Flange", "Channel", "Angle" ), label="Browse for section size", default="W8x24" )
Group_title13 = Frame( Dialog0, "Group title" )
Column14 = Column( Group_title13 )
Column15 = Column( Column14 )
Tabset16 = Tabset( Column15 )
Tab17 = Tab( Tabset16, "Tab title" )
Tab18 = Tab( Tabset16, "Tab title" )
File_save19 = ChooseFile( Dialog0, 'file_name', label="Enter filename", save=True, default="/path" )
screen_data = Dialog0.done()
# Dialog end

